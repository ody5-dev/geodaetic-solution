# -*- coding: utf-8 -*-
"""
Created on Fri Jul 10 21:57:00 2020

@author: JP
"""

from gravipy.tensorial import * 
import sympy as sym
from sympy import init_printing
import inspect
init_printing()
import numpy as np
import math

import scipy.integrate
import matplotlib.pyplot as plt
import matplotlib.animation as ani
import seaborn as sns
sns.set('talk')#, 'whitegrid', 'bright')


# define some symbolic variables
t, r, theta, phi, M = symbols('t, r, \\theta, \phi, M')
# create a coordinate four-vector object instantiating 
# the Coordinates class
x = Coordinates('x', [t, r, theta, phi])
# define a matrix of a metric tensor components
Metric = diag((1-2*M/r), -1/(1-2*M/r), -r**2, -r**2*sin(theta)**2)
#Metric = diag(A, -B, -r**2, -r**2*sin(theta)**2)
# create a metric tensor object instantiating the MetricTensor class
g = MetricTensor('g', x, Metric)

# If needed:
#Ga = Christoffel('Ga', g)
#Rm = Riemann('Rm', g)
#Ri = Ricci('Ri', g)
#G = Einstein('G', Ri)


# Get geodaetic equation
tau = Symbol('\\tau')
w = Geodesic('w', g, tau)
w(All).transpose()

# Simplify by fixing theta to pi/2
DGL = w(All).subs([(x(-3).diff(tau,tau),0),
                   (x(-3).diff(tau),0),
                   (x(-3),math.pi/2)]).transpose()

# Simplify differential equation from second order to first order:
DGLa = sym.solve(DGL[0], x(-1).diff(tau,tau))[0]
DGLb = sym.solve(DGL[1], x(-2).diff(tau, tau))[0]
DGLd = sym.solve(DGL[3], x(-4).diff(tau, tau))[0]

y = symbols('y1, y2, y3, y4, y5, y6')
sym_m = symbols('M')
y1, y2, y3, y4, y5, y6 = y
substitutes = [
               (x(-1).diff(tau), y4), # y4 = dt/dtau
               (x(-2).diff(tau), y5), # y5 = dr/dtau
               (x(-4).diff(tau), y6), # y6 = dφ/dtau
               (x(-1), y1), # y1 = t
               (x(-2), y2), # y2 = r
               (x(-4), y3), # y3 = φ
               (sym_m, 1), # M = 1
              ]

DGLa_umgeschrieben = DGLa.subs(substitutes)
DGLb_umgeschrieben = DGLb.subs(substitutes)
DGLd_umgeschrieben = DGLd.subs(substitutes)


DGLa_neu = sym.lambdify([tau, y], DGLa_umgeschrieben, modules=sym)
DGLb_neu = sym.lambdify([tau, y], DGLb_umgeschrieben, modules=sym)
DGLd_neu = sym.lambdify([tau, y], DGLd_umgeschrieben, modules=sym)
DGLy4 = sym.lambdify([tau, y], y4, modules=sym)
DGLy5 = sym.lambdify([tau, y], y5, modules=sym)
DGLy6 = sym.lambdify([tau, y], y6, modules=sym)

def EbeneDGL(tau, y):
  dy1 = DGLy4(tau, y)
  dy2 = DGLy5(tau, y)
  dy3 = DGLy6(tau, y)
  dy4 = DGLa_neu(tau, y)
  dy5 = DGLb_neu(tau, y)
  dy6 = DGLd_neu(tau, y)
  return np.array([dy1, dy2, dy3, dy4, dy5, dy6])

l0 = 4.47
r0 = 9
t0 = 0
phi0 = 0
dr0 = 0.00
dphi0 = l0/r0**2
dt0 = 1/np.sqrt(1-2/r0)*np.sqrt(1+r0**2*dphi0**2)

VeffRez = lambda r,M,l: np.sqrt(1-2*M/r)*np.sqrt(1+l**2/r**2)
l = r0**2*dphi0
e0 = dt0*(1-2/r0)

r_span = np.linspace(4, 60, 500)
Veff = VeffRez(r_span, 1, l)


constraint = np.array([t0, r0, phi0,
                       dt0, dr0, dphi0])
tend = 8000
t = np.linspace(0, tend, 10001)
solution = scipy.integrate.solve_ivp(EbeneDGL, (0, tend), 
                                     constraint, t_eval=t)


r1 = solution['y'][1,:]
phi1 = solution['y'][2,:]


import matplotlib.animation as ani

fig, (ax1, ax3) = plt.subplots(1, 2, figsize=(30,15))
line1, = ax1.plot([], [], '--', color='red', lw=1.5)
point1, = ax1.plot([], [], marker='o', color='red', markersize=15)
center, = ax1.plot([0], [0], marker='o', color='black', markersize=25)

#line2, = ax2.plot([], [], '--', color='red', lw=1.5)
#point2, = ax2.plot([], [], marker='o', color='red')

Veff = VeffRez(r_span, 1, l)
line3_bg, = ax3.plot(r_span, Veff, '-', color='blue', lw=1)
point3, = ax3.plot([], [], marker='o', color='red')

ax1.set_xlim(-r1.max(), r1.max())
ax1.set_ylim(-r1.max(), r1.max())
ax1.set_xlabel('$x$')
ax1.set_ylabel('$y$')
ax1.set_title('Top View')

#ax2.set_xlim(r1.min(), r1.max())
#ax2.set_ylim(phi1.min(), phi1.max())
#ax2.set_xlabel('$r$')
#ax2.set_ylabel('$\\phi$')
#ax2.set_title('Polar Coord')

ax3.set_xlabel('$r$')
ax3.set_ylabel('$V_{eff}(r)$')
ax3.set_title('Effective Potential')

def init():
  line1.set_data([], [])
  point1.set_data([], [])

  #  line2.set_data([], [])
  #  point2.set_data([], [])

  point3.set_data([], [])
  
  return (line1, point1, point3)

def animation_func(i):
  x1 = r1[:5*i]*np.cos(phi1[:5*i])
  y1 = r1[:5*i]*np.sin(phi1[:5*i])

  line1.set_data(x1, y1)
  if i > 0: point1.set_data(x1[-1], y1[-1])

  # line2.set_data(r1[:5*i], phi1[:5*i])
  # if i > 0: point2.set_data(r1[5*i], phi1[5*i])

  point3.set_data(r1[5*i], e0)
  
  return (line1, point1, point3)

anim = ani.FuncAnimation(fig, animation_func, init_func=init, 
                     frames=2000, interval=60, blit=True)

anim.save('geodaetic_solution.mp4')

#anim
#plt.show()

# Plot solution in a second plot
fig, axes = plt.subplots(2,3, figsize=(15,10))
for i in range(3):
  name = x(-(i+1)) if i < 2 else x(-(i+2))
  axes[0, i].plot(solution['t'], solution['y'][i,:], '-', label='$%s$' % name)
  axes[0, i].set_xlabel('$\\tau$')
  axes[0, i].set_ylabel('$%s$' % name)

r_span = np.linspace(4, 15, 500)
Veff = VeffRez(r_span, 1, l)
axes[1,0].plot(r_span, Veff, label='Effective Potential')
axes[1,0].plot(r0, VeffRez(r0, 1, l), 'ro')
axes[1,0].set_xlabel('$r$')
axes[1,0].set_ylabel('$V(r)$')

r1 = solution['y'][1,:]
phi1 = solution['y'][2,:]
axes[1,1].plot(r1, phi1, '-', label='$\\phi(r)$')
axes[1,1].set_xlabel('$r$')
axes[1,1].set_ylabel('$\\phi$')

x1 = r1*np.cos(phi1)
y1 = r1*np.sin(phi1)
axes[1,2].plot(x1, y1, '-', label='$x(\\tau)$')
axes[1,2].plot([0], [0], 'o', color='black', markersize=20)
axes[1,2].set_xlabel('$x$')
axes[1,2].set_ylabel('$y$')

for ax in axes.ravel():
  ax.legend()

plt.savefig("geodaetic_solution.png")
plt.show()