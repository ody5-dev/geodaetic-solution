FROM ubuntu:18.04

ENV OPENCV_VERSION 3.4.0

RUN apt-get update && apt-get install -y --no-install-recommends \
            git \
            ffmpeg \
            python3 \
            python3-dev \
            python3-pip \
            wget \
    && pip3 install -U pip setuptools wheel \
    && hash -r \
    && pip3 install gravipy numpy scipy seaborn ffmpeg opencv-python \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
